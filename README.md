# NXC4EV3-GUI - .NET graphical interface for NXC4EV3

This is a graphical frontend for NXC4EV3 written in .NET.

## Install directory hierarchy
```
# Common:
nxc4ev3/nxc4ev3-gui.exe         # this program
nxc4ev3/nxc4ev3-gui.exe.config  # CLR 2.0 configuration
nxc4ev3/nxclib/                 # NXClib + EV3-API; hierarchy generated by nxclib build shell script
nxc4ev3/arm-2009q1/             # GCC for EV3; see links below

# Windows only:
nxc4ev3/nxc2cc.exe
nxc4ev3/ev3duder.exe
nxc4ev3/libstdc++-6.dll
nxc4ev3/libgcc_s_sjlj-1.dll

# Linux only:
nxc4ev3/nxc2cc
nxc4ev3/ev3duder

```
## GCC download URL
Found on [mindboards/ev3sources Wiki](https://github.com/mindboards/ev3sources/wiki/Getting-Started).

[Windows binary package](https://sourcery.mentor.com/public/gnu_toolchain/arm-none-linux-gnueabi/arm-2009q1-203-arm-none-linux-gnueabi-i686-mingw32.tar.bz2)

[Linux binary package](https://sourcery.mentor.com/public/gnu_toolchain/arm-none-linux-gnueabi/arm-2009q1-203-arm-none-linux-gnueabi-i686-pc-linux-gnu.tar.bz2)

[Source package](https://sourcery.mentor.com/public/gnu_toolchain/arm-none-linux-gnueabi/arm-2009q1-203-arm-none-linux-gnueabi.src.tar.bz2)

You are supposed to delete unneeded files, as 1 GB is insane.

## Build requirements
* CLR 2.0 capable compiler
* System.Windows.Forms implementation

## Tested build configuration
* Build system: Ubuntu 16.04.1
  * Mono 4.6.2.0
* Build system: Windows XP Professional SP3
  * Visual Studio 2010 SP1 (version 11.0)

## Copyright

```
NXC4EV3-GUI - the graphical frontend for NXC4EV3

Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague

```
