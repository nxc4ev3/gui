﻿/**
 * NXC4EV3-GUI - the graphical frontend for NXC4EV3
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * External program execution module.
 */

using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace nxc4ev3_gui {
	public class ProcessRunner {
		private TextBox box;

		public ProcessRunner (TextBox box) {
			this.box = box;
		}

		public Process CreateProcess (string path, string args) {
			Process proc = new Process ();
			proc.StartInfo.FileName = path;
			proc.StartInfo.Arguments = args;
			proc.StartInfo.CreateNoWindow = true;
			proc.StartInfo.UseShellExecute = false;
			proc.StartInfo.RedirectStandardOutput = true;
			proc.StartInfo.RedirectStandardError = true;
			proc.StartInfo.RedirectStandardInput = true;
			proc.OutputDataReceived += stdout_redirect;
			proc.ErrorDataReceived += stderr_redirect;
			return proc;
		}

		public Process CreateProcess (ExeInfo info, string nxcFile, string resDir, string outDir) {
			return CreateProcess (info.filename, info.arg_fn (nxcFile, resDir, outDir));
		}

		public bool CreateProcessRun (string path, string args, string desc) {
			Process proc = CreateProcess (path, args);
			return RunProcess (proc, desc);
		}

		public bool CreateProcessRun (ExeInfo info, string nxcFile, string resDir, string outDir) {
			Process proc = CreateProcess (info, nxcFile, resDir, outDir);
			return RunProcess (proc, info.description);
		}

		public void Append (string data) {
			box.Invoke ((MethodInvoker)delegate {
				box.AppendText (data + Environment.NewLine);
			});
		}

		public void Clean () {
			box.Invoke ((MethodInvoker)delegate {
				box.Text = MainForm.LogHead;
			});
		}

		private void stderr_redirect (object sender, DataReceivedEventArgs e) {
			if (e != null && e.Data != null && e.Data.Length > 0)
				Append ("[exec]   " + e.Data);
		}

		private void stdout_redirect (object sender, DataReceivedEventArgs e) {
			if (e != null && e.Data != null && e.Data.Length > 0)
				Append ("[exec]   " + e.Data);
		}

		public bool RunProcess (Process proc, string name) {
			Append ("[exec] Running " + name + "...");
			try {
				proc.Start ();
			} catch (Exception e) {
				Append (e.Message + Environment.NewLine + e.StackTrace);
				return false;
			}
			proc.BeginErrorReadLine ();
			proc.BeginOutputReadLine ();
			proc.WaitForExit ();
			if (proc.ExitCode != 0) {
				Append ("[exec] Error!");
				return false;
			} else {
				Append ("[exec] OK!");
				return true;
			}
		}

	}
}

