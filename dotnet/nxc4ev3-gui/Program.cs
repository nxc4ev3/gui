﻿/**
 * NXC4EV3-GUI - the graphical frontend for NXC4EV3
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Main entry point.
 */

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace nxc4ev3_gui {
	static class Program {
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main () {
			// nxc2cc temporary file
			Environment.CurrentDirectory = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
			Application.EnableVisualStyles ();
			Application.SetCompatibleTextRenderingDefault (false);
			Application.Run (new MainForm ());
		}
	}
}
