﻿/**
 * NXC4EV3-GUI - the graphical frontend for NXC4EV3
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Arguments module
 *  - builds command line parameters for programs.
 */

using System;
using System.IO;
using System.Text;

namespace nxc4ev3_gui {

	public delegate string GetArguments (string nxcFile, string resDir, string outDir);

	public static class Flags {
		public static string GCCFlags (string nxc, string res, string outdir) {
			return String.Format (
			 // " -static" +
				" -std=gnu99" +
				" -Os -s" +
				" -pthread" +
				" -isystem {0}" +
				" {1}" +
				" {2}" +
				" {3}" +
				" -lm -lrt" +
				" -o {4}",
				EscapeArg (Files.NXCLIB_C),
				EscapeArg (Files.OutFileC (nxc, outdir)),
				EscapeArg (Path.Combine (Files.NXCLIB_CLIB, "nxclib.a")),
				EscapeArg (Path.Combine (Files.NXCLIB_CLIB, "libev3api.a")),
				EscapeArg (Files.OutFileELF (nxc, outdir))
			);
		}

		public static string DuderFlagsRbf (string nxc, string res, string outdir) {
			return String.Format (" mkrbf {0} {1}",
				EscapeArg (Files.RemoteProjFromBase (nxc, ".elf")),
				EscapeArg (Files.OutFileRBF (nxc, outdir))
			);
		}

		public static string DuderFlagsMkdir (string nxc, string res, string outdir) {
			string remote = String.Format (
				                "{0}/{1}",
				                Files.RemoteProjs,
				                Files.OutFileBase (nxc)
			                );
			return String.Format (" mkdir {0}",
				EscapeArg (remote)
			);
		}

		public static string DuderFlagsUp (string nxc, string local) {
			string proj = Files.OutFileBase (nxc);
			string filename = Path.GetFileName (local);
			return String.Format (" up {0} {1}",
				EscapeArg (local),
				EscapeArg (Files.RemoteFile(proj, filename))
			);
		}
		public static string DuderFlagsElfUp (string nxc, string res, string outdir) {
			return DuderFlagsUp (nxc, Files.OutFileELF(nxc, outdir));
		}

		public static string DuderFlagsRbfUp (string nxc, string res, string outdir) {
			return DuderFlagsUp (nxc, Files.OutFileRBF(nxc, outdir));
		}

		public static string NXC2CCFlags (string nxc, string res, string outdir) {
			return String.Format (
				" {0} {1} {2} {3} {4}",
				EscapeArg (Files.NXCLIB_NXC),
				EscapeArg (res),
				EscapeArg (nxc),
				EscapeArg (Files.OutFileC (nxc, outdir)),
				EscapeArg (Files.OutFileDL (nxc, outdir))
			);
		}

		public static string EscapeArg (string filename) {
			StringBuilder build = new StringBuilder ();
			build.Append ("\"");

			for (int i = 0; i < filename.Length; i++) {
				int backslashes = 0;
				while (i < filename.Length && filename [i] == '\\') {
					backslashes++;
					i++;
				}

				if (i == filename.Length) {
					build.Append ('\\', backslashes * 2);
					break;
				} else if (filename [i] == '"') {
					build.Append ('\\', backslashes * 2 + 1);
					build.Append ('"');
				} else {
					build.Append ('\\', backslashes);
					build.Append (filename [i]);
				}

			}

			build.Append ("\"");
			return build.ToString ();

		}
	}
}

