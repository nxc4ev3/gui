﻿/**
 * NXC4EV3-GUI - the graphical frontend for NXC4EV3
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Filesystem module
 *  - determines paths to various files
 */


using System;
using System.IO;

namespace nxc4ev3_gui {
	/// <summary>
	/// Filesystem module
	/// </summary>
	public static class Files {
		/// <summary>
		/// Check if this program is running on a POSIX system.
		/// </summary>
		public static bool IsLinux {
			get {
				int p = (int)Environment.OSVersion.Platform;
				return (p == 4) || (p == 6) || (p == 128);
			}
		}


		public static string RemoteFile(string proj, string filename) {
			return String.Format (
				"{0}/{1}/{2}",
				RemoteProjs,
				proj,
				filename
			);
		}
		/// <summary>
		/// Get the file path for the specific project file.
		/// </summary>
		public static string RemoteProj (string proj, string ext) {
			return RemoteFile (proj, proj + ext);
		}

		/// <summary>
		/// Get the file path for the specific project file.
		/// </summary>
		public static string RemoteProjFromBase (string nxc, string ext) {
			return RemoteProj (OutFileBase (nxc), ext);
		}


		/// <summary>
		/// Get the filename prefix for generated files.
		/// </summary>
		public static string OutFileBase (string nxc) {
			return Path.GetFileNameWithoutExtension (nxc);
		}

		/// <summary>
		/// Get the file path for the specific file type.
		/// </summary>
		public static string OutFile (string outdir, string nxc, string ext) {
			string filename = OutFileBase (nxc) + ext;
			return Path.Combine (outdir, filename);
		}

		/// <summary>
		/// Get the file path for the generated C file.
		/// </summary>
		public static string OutFileC (string nxc, string outdir) {
			return OutFile (outdir, nxc, ".c");
		}

		/// <summary>
		/// Get the file path for the generated download listing.
		/// </summary>
		public static string OutFileDL (string nxc, string outdir) {
			return OutFile (outdir, nxc, ".dl");
		}

		/// <summary>
		/// Get the file path for the generated executable file.
		/// </summary>
		public static string OutFileELF (string nxc, string outdir) {
			return OutFile (outdir, nxc, ".elf");
		}

		/// <summary>
		/// Get the file path for the generated EV3 wrapper.
		/// </summary>
		public static string OutFileRBF (string nxc, string outdir) {
			return OutFile (outdir, nxc, ".rbf");
		}


		/// <summary>
		/// Get the elative root for the required software.
		/// </summary>
		public static string RelativeRoot {
			get {
				return AppDomain.CurrentDomain.BaseDirectory;
			}
		}

		/// <summary>
		/// Get the root directory of the GNU C Compiler for EV3.
		/// </summary>
		public static string GCCRoot {
			get {
				return Path.Combine (
					RelativeRoot,
					"arm-2009q1");
			}
		}

		/// <summary>
		/// Get the path to the GCC executable.
		/// </summary>
		public static string GCC {
			get {
				return Path.Combine (
					Path.Combine (GCCRoot, "bin"),
					IsLinux ? "arm-none-linux-gnueabi-gcc" : "arm-none-linux-gnueabi-gcc.exe");
			}
		}

		/// <summary>
		/// Get the path to the NXC2CC executable.
		/// </summary>
		public static string NXC2CC {
			get {
				return Path.Combine (
					RelativeRoot,
					IsLinux ? "nxc2cc" : "nxc2cc.exe");
			}
		}

		/// <summary>
		/// Get the path to the the EV3 Downloader/UploaDER.
		/// </summary>
		public static string EV3DUDER {
			get {
				return Path.Combine (
					RelativeRoot,
					IsLinux ? "ev3duder" : "ev3duder.exe");
			}
		}

		/// <summary>
		/// Get the root directory of nxclib library.
		/// </summary>
		public static string NXCLIB {
			get {
				return Path.Combine (
					RelativeRoot,
					"nxclib");
			}
		}

		/// <summary>
		/// Get the path to the nxclib c-include directory.
		/// </summary>
		public static string NXCLIB_C {
			get {
				return Path.Combine (
					NXCLIB,
					"include.c");
			}
		}

		/// <summary>
		/// Get the path to the nxclib c-library directory.
		/// </summary>
		public static string NXCLIB_CLIB {
			get {
				return Path.Combine (
					NXCLIB,
					"lib.c");
			}
		}

		/// <summary>
		/// Get the path to the nxclib nxc-include directory.
		/// </summary>
		public static string NXCLIB_NXC {
			get {
				return Path.Combine (
					NXCLIB,
					"include.nxc");
			}
		}

		/// <summary>
		/// Get the EV3 project root path.
		/// </summary>
		public static string RemoteProjs {
			get {
				return "/home/root/lms2012/prjs";
			}
		}
	}
}
