﻿/**
 * NXC4EV3-GUI - the graphical frontend for NXC4EV3
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Main form module.
 */

using System;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using DlgResult = System.Windows.Forms.DialogResult;

namespace nxc4ev3_gui {
	public partial class MainForm : Form {
		public static readonly string LogHead =
			"Command output will be shown here." + Environment.NewLine;
		Thread execThread = null;
		ProcessRunner run;

		public MainForm () {
			InitializeComponent ();
			run = new ProcessRunner (outputbox);

			outputbox.Text = LogHead;
		}

		////////////////////
		// EVENT HANDLERS //
		////////////////////
		private void nxcfilebutton_Click (object sender, EventArgs e) {
			DlgResult res = nxcfiledialog.ShowDialog (this);
			if (res == DlgResult.OK) {
				string filename = nxcfiledialog.FileName;
				nxcfilebox.Text = filename;
				if (resdirbox.Text.Length == 0) {
					string dirname = Directory.GetParent (filename).FullName;
					resdirbox.Text = dirname;
					resdirdialog.SelectedPath = dirname;
				}
			}
		}

		private void resdirbutton_Click (object sender, EventArgs e) {
			DlgResult res = resdirdialog.ShowDialog (this);
			if (res == DlgResult.OK) {
				string dirname = resdirdialog.SelectedPath;
				resdirbox.Text = dirname;
			}
		}

		private void compilebutton_Click (object sender, EventArgs e) {
			if (!canStart ())
				return;
			branchThread (build);
		}

		private void uploadbutton_Click (object sender, EventArgs e) {
			if (!canStart ())
				return;
			branchThread (upload);
		}

		private bool canStart () {
			if (!File.Exists (NXCFile)) {
				run.Append ("Error: NXC file not found.");
				return false;
			}
			if (!Directory.Exists (ResDir)) {
				run.Append ("Error: resource directory not found.");
				return false;
			}
			return true;
		}

		///////////////
		// FILENAMES //
		//////////////

		string OutDir {
			get {
				return Directory.GetParent (NXCFile).FullName;
			}
		}

		string NXCFile {
			get {
				return nxcfilebox.Text;
			}
		}

		string ResDir {
			get {
				return resdirbox.Text;
			}
		}

		///////////////////////////////////
		// EXEC THREAD UTILITY FUNCTIONS //
		///////////////////////////////////

		void branchThread (ThreadStart func) {
			if (execThread == null || !execThread.IsAlive) {
				execThread = new Thread (func);
				execThread.Start ();
			} else {
				run.Append ("Cannot start: another operation is running.");
			}
		}


		///////////////////////////
		// EXEC THREAD FUNCTIONS //
		///////////////////////////

		void build () {
			run.Clean ();
			if (!invoke (ExeType.nxc2cc)) {
				return;
			} else {
				run.Append ("[info] C output can be found here: " + Files.OutFileC (NXCFile, OutDir));
				run.Append ("");
			}

			if (!invoke (ExeType.gcc)) {
				return;
			} else {
				run.Append ("[info] ELF binary can be found here: " + Files.OutFileELF (NXCFile, OutDir));
				run.Append ("");
			}
		}

		void upload () {
			run.Clean ();
			if (!invoke (ExeType.ev3duder_mkrbf)) {
				return;
			} else {
				run.Append ("[info] RBF wrapper can be found here: " + Files.OutFileRBF (NXCFile, OutDir));
				run.Append ("");
			}

			// mkdir can fail
			if (!invoke (ExeType.ev3duder_mkdir)) {
				run.Append("[status] --- MKDIR failed - Previous program found. This failure is harmless. ---");
			} else {
				run.Append("[status] --- MKDIR succeeded - Previous program not found. ---");
			}

			if (!invoke (ExeType.ev3duder_up_elf)) {
				run.Append("[status] --- Native file upload failed. ---");
				return;
			} else {
				run.Append("[status] --- Native file upload succeeded. ---");
			}
			if (!invoke (ExeType.ev3duder_up_rbf)) {
				run.Append("[status] --- EV3 wrapper upload failed. ---");
				return;
			} else {
				run.Append("[status] --- EV3 wrapper upload succeeded. ---");
			}

			uploadUser ();
		}

		void uploadUser() {
			using (StreamReader read = File.OpenText (Files.OutFileDL (NXCFile, OutDir))) {
				while (!read.EndOfStream) {
					string file = read.ReadLine ();
					file = file.Trim ();
					if (file.Length == 0)
						continue;
					string flags = Flags.DuderFlagsUp (NXCFile, file);
					if (run.CreateProcessRun (Files.EV3DUDER, flags, "EV3DUDER upload " + file)) {
						run.Append("[status] --- Upload of file [" + file + "] succeeded. ---");
					} else {
						run.Append("[status] --- Upload of file [" + file + "] failed. ---");
					}
				}
			}
		}


		///////////////////////////
		// PROGRAM CALL DISPATCH //
		///////////////////////////

		static readonly Dictionary<ExeType, ExeInfo> calls;

		static MainForm () {
			calls = new Dictionary<ExeType, ExeInfo> ();
			calls.Add (ExeType.nxc2cc,          new ExeInfo (Files.NXC2CC,   Flags.NXC2CCFlags,     "NXC2CC"));
			calls.Add (ExeType.gcc,             new ExeInfo (Files.GCC,      Flags.GCCFlags,        "GCC"));
			calls.Add (ExeType.ev3duder_mkrbf,  new ExeInfo (Files.EV3DUDER, Flags.DuderFlagsRbf,   "EV3DUDER mkrbf"));
			calls.Add (ExeType.ev3duder_mkdir,  new ExeInfo (Files.EV3DUDER, Flags.DuderFlagsMkdir, "EV3DUDER mkdir"));
			calls.Add (ExeType.ev3duder_up_elf, new ExeInfo (Files.EV3DUDER, Flags.DuderFlagsElfUp, "EV3DUDER upload ELF"));
			calls.Add (ExeType.ev3duder_up_rbf, new ExeInfo (Files.EV3DUDER, Flags.DuderFlagsRbfUp, "EV3DUDER upload RBF"));
		}

		bool invoke (ExeType type) {
			return run.CreateProcessRun (calls [type], NXCFile, ResDir, OutDir);
		}
	}

	public enum ExeType {
		nxc2cc,
		gcc,
		ev3duder_mkrbf,
		ev3duder_mkdir,
		ev3duder_up_elf,
		ev3duder_up_rbf
	}


	public class ExeInfo {
		public string filename;
		public GetArguments arg_fn;
		public string description;

		public ExeInfo (string filename, GetArguments arg_fn, string description) {
			this.filename = filename;
			this.arg_fn = arg_fn;
			this.description = description;
		}
	}
}
