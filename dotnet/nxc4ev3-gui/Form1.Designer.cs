﻿/**
 * NXC4EV3-GUI - the graphical frontend for NXC4EV3
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * WinForms designer output.
 */

namespace nxc4ev3_gui {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent () {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.actionFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.uploadbutton = new System.Windows.Forms.Button();
            this.compilebutton = new System.Windows.Forms.Button();
            this.outputbox = new System.Windows.Forms.TextBox();
            this.nxclabel = new System.Windows.Forms.Label();
            this.reslabel = new System.Windows.Forms.Label();
            this.nxcfilebox = new System.Windows.Forms.TextBox();
            this.resdirbox = new System.Windows.Forms.TextBox();
            this.nxcfilebutton = new System.Windows.Forms.Button();
            this.resdirbutton = new System.Windows.Forms.Button();
            this.resdirdialog = new System.Windows.Forms.FolderBrowserDialog();
            this.nxcfiledialog = new System.Windows.Forms.OpenFileDialog();
            this.mainTable.SuspendLayout();
            this.actionFlowPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.ColumnCount = 3;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.mainTable.Controls.Add(this.actionFlowPanel, 0, 2);
            this.mainTable.Controls.Add(this.outputbox, 0, 3);
            this.mainTable.Controls.Add(this.nxclabel, 0, 0);
            this.mainTable.Controls.Add(this.reslabel, 0, 1);
            this.mainTable.Controls.Add(this.nxcfilebox, 1, 0);
            this.mainTable.Controls.Add(this.resdirbox, 1, 1);
            this.mainTable.Controls.Add(this.nxcfilebutton, 2, 0);
            this.mainTable.Controls.Add(this.resdirbutton, 2, 1);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(0, 0);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 4;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.Size = new System.Drawing.Size(417, 295);
            this.mainTable.TabIndex = 1;
            // 
            // actionFlowPanel
            // 
            this.mainTable.SetColumnSpan(this.actionFlowPanel, 3);
            this.actionFlowPanel.Controls.Add(this.uploadbutton);
            this.actionFlowPanel.Controls.Add(this.compilebutton);
            this.actionFlowPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionFlowPanel.Location = new System.Drawing.Point(0, 56);
            this.actionFlowPanel.Margin = new System.Windows.Forms.Padding(0);
            this.actionFlowPanel.Name = "actionFlowPanel";
            this.actionFlowPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.actionFlowPanel.Size = new System.Drawing.Size(417, 28);
            this.actionFlowPanel.TabIndex = 1;
            // 
            // uploadbutton
            // 
            this.uploadbutton.Location = new System.Drawing.Point(339, 3);
            this.uploadbutton.Name = "uploadbutton";
            this.uploadbutton.Size = new System.Drawing.Size(75, 23);
            this.uploadbutton.TabIndex = 0;
            this.uploadbutton.Text = "Upload";
            this.uploadbutton.UseVisualStyleBackColor = true;
            this.uploadbutton.Click += new System.EventHandler(this.uploadbutton_Click);
            // 
            // compilebutton
            // 
            this.compilebutton.Location = new System.Drawing.Point(258, 3);
            this.compilebutton.Name = "compilebutton";
            this.compilebutton.Size = new System.Drawing.Size(75, 23);
            this.compilebutton.TabIndex = 1;
            this.compilebutton.Text = "Compile";
            this.compilebutton.UseVisualStyleBackColor = true;
            this.compilebutton.Click += new System.EventHandler(this.compilebutton_Click);
            // 
            // outputbox
            // 
            this.mainTable.SetColumnSpan(this.outputbox, 3);
            this.outputbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputbox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputbox.Location = new System.Drawing.Point(3, 87);
            this.outputbox.Multiline = true;
            this.outputbox.Name = "outputbox";
            this.outputbox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputbox.Size = new System.Drawing.Size(411, 205);
            this.outputbox.TabIndex = 4;
            // 
            // nxclabel
            // 
            this.nxclabel.AutoSize = true;
            this.nxclabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nxclabel.Location = new System.Drawing.Point(3, 0);
            this.nxclabel.Name = "nxclabel";
            this.nxclabel.Size = new System.Drawing.Size(74, 28);
            this.nxclabel.TabIndex = 5;
            this.nxclabel.Text = "NXC file:";
            this.nxclabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // reslabel
            // 
            this.reslabel.AutoSize = true;
            this.reslabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reslabel.Location = new System.Drawing.Point(3, 28);
            this.reslabel.Name = "reslabel";
            this.reslabel.Size = new System.Drawing.Size(74, 28);
            this.reslabel.TabIndex = 6;
            this.reslabel.Text = "Resource dir:";
            this.reslabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nxcfilebox
            // 
            this.nxcfilebox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nxcfilebox.Location = new System.Drawing.Point(83, 3);
            this.nxcfilebox.Name = "nxcfilebox";
            this.nxcfilebox.Size = new System.Drawing.Size(301, 20);
            this.nxcfilebox.TabIndex = 7;
            // 
            // resdirbox
            // 
            this.resdirbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resdirbox.Location = new System.Drawing.Point(83, 31);
            this.resdirbox.Name = "resdirbox";
            this.resdirbox.Size = new System.Drawing.Size(301, 20);
            this.resdirbox.TabIndex = 8;
            // 
            // nxcfilebutton
            // 
            this.nxcfilebutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nxcfilebutton.Location = new System.Drawing.Point(390, 3);
            this.nxcfilebutton.Name = "nxcfilebutton";
            this.nxcfilebutton.Size = new System.Drawing.Size(24, 22);
            this.nxcfilebutton.TabIndex = 9;
            this.nxcfilebutton.Text = "...";
            this.nxcfilebutton.UseVisualStyleBackColor = true;
            this.nxcfilebutton.Click += new System.EventHandler(this.nxcfilebutton_Click);
            // 
            // resdirbutton
            // 
            this.resdirbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resdirbutton.Location = new System.Drawing.Point(390, 31);
            this.resdirbutton.Name = "resdirbutton";
            this.resdirbutton.Size = new System.Drawing.Size(24, 22);
            this.resdirbutton.TabIndex = 10;
            this.resdirbutton.Text = "...";
            this.resdirbutton.UseVisualStyleBackColor = true;
            this.resdirbutton.Click += new System.EventHandler(this.resdirbutton_Click);
            // 
            // nxcfiledialog
            // 
            this.nxcfiledialog.DefaultExt = "nxc";
            this.nxcfiledialog.Filter = "NXC source code (*.nxc)|*.nxc|All files (*.*)|*.*";
            this.nxcfiledialog.Title = "Select NXC input file";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 295);
            this.Controls.Add(this.mainTable);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "NXC4EV3";
            this.mainTable.ResumeLayout(false);
            this.mainTable.PerformLayout();
            this.actionFlowPanel.ResumeLayout(false);
            this.ResumeLayout(false);

		}

#endregion

		private System.Windows.Forms.TableLayoutPanel mainTable;
		private System.Windows.Forms.FlowLayoutPanel actionFlowPanel;
		private System.Windows.Forms.Button uploadbutton;
		private System.Windows.Forms.Button compilebutton;
		private System.Windows.Forms.TextBox outputbox;
		private System.Windows.Forms.Label nxclabel;
		private System.Windows.Forms.Label reslabel;
		private System.Windows.Forms.TextBox nxcfilebox;
		private System.Windows.Forms.TextBox resdirbox;
		private System.Windows.Forms.Button nxcfilebutton;
		private System.Windows.Forms.Button resdirbutton;
		private System.Windows.Forms.FolderBrowserDialog resdirdialog;
		private System.Windows.Forms.OpenFileDialog nxcfiledialog;

	}
}

